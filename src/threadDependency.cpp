//============================================================================
// Name        : threadDependency.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <syscall.h>
#include <cxxabi.h>
#include <climits>
#include <map>

const int THR_ERR_CODE(111);
const int THR_OK_CODE(101);
pthread_mutex_t m_mutex;



/*** Thread Info and Array *******************************************************************/
class ThreadInfo
{
  public:
    ThreadInfo():
        id(NULL),
        lwp(LONG_MAX),
        name(""){}
    ThreadInfo ( pthread_t* _id, const long int& _lwp, const std::string& _name )
    {
        id  = _id;
        lwp = _lwp;
        name= _name;
    }

    pthread_t* id;
    long int lwp;
    std::string name;

    friend std::ostream& operator<<( std::ostream&, const ThreadInfo& );
};

std::ostream& operator<< ( std::ostream& os, const ThreadInfo& thrId )
{
    return os << "Thread 0x" << std::hex << std::uppercase  << *( thrId.id )
              << std::dec << std::uppercase
              << " (LWP "    << thrId.lwp  << ") "
              << "\""        << thrId.name << "\"";
}

/**
 * @brief ThreadInfoArrayis design to monitor the threads
 *
 * The threadInfoArray class is a shared ressource.
 * The read, write access to the array is managed by a mutex.
 */
class ThreadInfoArray
{
public:
    ThreadInfoArray()
    {
        m_mutex
    }

    void push_back(ThreadInfo threadInfo)
    {
        m_thrArray.push_back( threadInfo );
    }

    void setLwp( pthread_t thrId= pthread_self() )
    {
        std::vector< ThreadInfo >::iterator iter= search( thrId );
        if( iter == m_thrArray.end() )
        {
            std::cout << __FUNCTION__ << ": " << thrId << " not found in the thread array." << std::endl;
            printThrArray();
        }
        else{
                iter->lwp= syscall( SYS_gettid );
        }
    }

    void removeThread( pthread_t thrId= pthread_self() )
    {
        std::vector< ThreadInfo >::iterator iter= search( thrId );
        if( iter == m_thrArray.end() )
        {
            std::cout << __FUNCTION__ << ": " << thrId << " not found in the thread array." << std::endl;
            printThrArray();
        }
        else{
                m_thrArray.erase(iter);
        }
    }

    void printThrArray()
    {
        for( std::vector< ThreadInfo >::iterator iter =  m_thrArray.begin();
             iter != m_thrArray.end();
             iter++)
        {
            std::cout << *iter << std::endl;
        }
    }

    ThreadInfo getInfo( pthread_t thrId= pthread_self() )
    {
        ThreadInfo thrInfo;

        std::vector< ThreadInfo >::iterator iter= search( thrId );
        if( iter == m_thrArray.end() )
        {
            std::cout << __FUNCTION__ << ": " << thrId << " not found in the thread array." << std::endl;
            printThrArray();
        }
        else{
            thrInfo= *iter;
        }

        return thrInfo;
    }

    std::vector< ThreadInfo >::iterator search( pthread_t thrId= pthread_self() )
    {
        std::vector< ThreadInfo >::iterator iter= m_thrArray.end();

        for( iter =  m_thrArray.begin();
             iter != m_thrArray.end();
             iter++)
        {
            if (    (  (*iter).id != NULL )
                 && ( *(*iter).id == thrId ) )
            {
                return iter;
            }
        }
    }

private:
    pthread_mutex_t m_mutex;
    std::vector< ThreadInfo > m_thrArray;
};


/*** Test exit thread with error *****************************************************************/
void *testexitWithErr(void *threadid)
{
	long* thrId= reinterpret_cast<long*>(threadid);
	long int tid= syscall(SYS_gettid);

	std::cout << "#" << tid << " - " << __FUNCTION__ << ": enter & exit thread (id: "
			  << *thrId  << ")" << std::endl;

	// Exit with err code
	pthread_exit((void*) &THR_ERR_CODE);
}

/*** Test crashing thread ************************************************************************/
void *testCrashingThread(void *threadid)
{
	long tid;
	tid = (long)threadid;
	printf("I'm the crashing thread #%ld!\n", tid);

	try{
		// crash of the thread
		std::string * ptrString=NULL;
		ptrString->size();
	}
	catch(std::exception& e)
	{std::cout << e.what() << std::endl;}
	printf("Terminate crashing thread #%ld!\n", tid);
	pthread_exit(NULL);
}

/*** Test clean up *****************************************************************************/
static void cleanup_handler(void *arg)
{
	std::cout << "#" << syscall(SYS_gettid) << " - Cleanup_handler is executed." << std::endl;
    // print exit originator
}

static void *testCleanUpThread(void *threadid)
{
	long int tid= syscall(SYS_gettid);
	std::cout << "#" << tid << " - testCleanUpThread: enter thread." << std::endl;

	pthread_cleanup_push(cleanup_handler, NULL);

	while(1)
	{
		std::cout << "#" << tid << " - testCleanUpThread: loop." << std::endl;
		sleep(2);
	}

	pthread_cleanup_pop(1);
	std::cout << "#" << tid << " - testCleanUpThread: Reached the return statement." << std::endl ;
	return NULL;
}


/*** Test clean up lock ************************************************************************/
static void cleanup_lock(void *arg)
{
	std::cout << "#" << syscall(SYS_gettid) << " - " << __FUNCTION__ << " handler is executed." << std::endl;

	pthread_mutex_t* mutex= (pthread_mutex_t*) arg;
	// if the mutex is locked, unlock it
	int retCode= pthread_mutex_trylock (mutex);
	pthread_mutex_unlock (mutex);

	if( retCode == 0 ){
		std::cout << "#" << syscall(SYS_gettid) << " - " << __FUNCTION__ << " Mutex was available and has been locked." << std::endl;
	}
	else if( retCode == EBUSY)
	{
		std::cout << "#" << syscall(SYS_gettid) << " - " << __FUNCTION__ << " Mutex is already locked." << std::endl;
	}
	else
	{
		std::cout << "#" << syscall(SYS_gettid) << " - " << __FUNCTION__ << " Mutex is not available: err code= " << retCode << std::endl;
	}
}

static void *testCleanUpLock(void *threadid)
{
	// Set the on exit callback
	pthread_cleanup_push(cleanup_lock, &m_mutex);

	long int tid= syscall(SYS_gettid);
	std::cout << "#" << tid << " - " << __FUNCTION__ << ": enter thread." << std::endl;

	// lock the mutex
	pthread_mutex_lock (&m_mutex);

	while(1)
	{
		std::cout << "#" << tid << " - " << __FUNCTION__ << ": loop." << std::endl;
		sleep(2);
	}

	pthread_cleanup_pop(1);
	std::cout << "#" << syscall(SYS_gettid) << " - " << __FUNCTION__ << ": Reached the return statement." << std::endl;

	return NULL;
}

/*** Farmer defintion *******************************************************************/
void *farmer(void *arg)
{
    long int tid= syscall(SYS_gettid);
//	std::cout << "#" << tid << " - " << __FUNCTION__ << ": enter thread." << std::endl;

    ThreadInfoArray* threadArray= static_cast< ThreadInfoArray* >( arg );
    threadArray->setLwp();

    std::cout << threadArray->getInfo() << ": Hi, I´m a farmer and I have work to do." << std::endl;

    // wait for pthread_exit signal
	try{
		while(1)
        {
            sleep(2);
            std::cout << threadArray->getInfo() << ": Still working." << std::endl;}
	}
    // thread termination can be catch using the thread stack unwind mechanism based on exceptions
	catch(abi::__forced_unwind& e)
	{
        std::cout << threadArray->getInfo() << ": __forced_unwind event received." << std::endl;
        threadArray->removeThread();
		throw;
    }
    catch(...){
        std::cout << threadArray->getInfo() << ": Unknown exception caught (and re-thrown)." << std::endl;
        threadArray->removeThread();
        throw;
    }
    std::cout << threadArray->getInfo() << ": Terminate crashing thread." << std::endl;
	pthread_exit(NULL);
}


/*********************************************/
/*              main                         */
/*********************************************/
int main (int argc, char *argv[])
{
    ThreadInfoArray threadArray;

	int rc;

	// init mutex
	pthread_mutex_init(&m_mutex, NULL);

	/*** Test of joinable thread returning err code **************************************
	pthread_t exitWithErr;
	pthread_attr_t thrAttr_exitWithErr;
	pthread_attr_init(&thrAttr_exitWithErr);
	pthread_attr_setdetachstate(&thrAttr_exitWithErr,PTHREAD_CREATE_JOINABLE);
	rc = pthread_create(&exitWithErr, &thrAttr_exitWithErr, testexitWithErr, &exitWithErr);
	pthread_attr_destroy(&thrAttr_exitWithErr);
	std::cout << "******* " << __FUNCTION__ << ": Test of thread returning err code." << std::endl;

	if (rc){
		printf("ERROR; return code from pthread_create() is %d\n", rc);
		exit(-1);
	}

	// Wait until thread termination with non blocking join
	// pthread_tryjoin_np return 0 if thread is terminated
	void *retCode;
	sleep(2);
	int joinRes= pthread_tryjoin_np(exitWithErr, &retCode);
	while( joinRes != 0 )
	{
		std::cout << __FUNCTION__ << ": Thread #" << exitWithErr
				  << " not terminated (err code= "<< joinRes << ") - wait 2 secs for termination." << std::endl;
		sleep(2);

		// Try to get the detached thread return code
		joinRes= pthread_tryjoin_np(exitWithErr, &retCode);
	}

	int* retcodeAsiInt= (int*) retCode;
	std::cout << __FUNCTION__ << ": Thread #" << exitWithErr
			  << " terminated with code: " << *retcodeAsiInt << std::endl;

	/*** Test of detach thread returning err code **************************************
	pthread_t exitWithErr;
	pthread_attr_t thrAttr_exitWithErr;
	pthread_attr_init(&thrAttr_exitWithErr);
	pthread_attr_setdetachstate(&thrAttr_exitWithErr,PTHREAD_CREATE_DETACHED);
	rc = pthread_create(&exitWithErr, &thrAttr_exitWithErr, testexitWithErr, &exitWithErr);
	pthread_attr_destroy(&thrAttr_exitWithErr);
	std::cout << "******* " << __FUNCTION__ << ": Test of thread returning err code." << std::endl;

	if (rc){
		printf("ERROR; return code from pthread_create() is %d\n", rc);
		exit(-1);
	}

	// Wait until thread termination with non blocking join
	// pthread_tryjoin_np return 0 if thread is terminated
	void *retCode;
	sleep(2);
	int joinRes= pthread_tryjoin_np(exitWithErr, &retCode);

	// A thread in detached state cannot be joined,
	// the EINVAL error code is expected
	if(joinRes == EINVAL){
		std::cout << __FUNCTION__ << ": Cannot join thread #" << exitWithErr
						  << ". The thread has been detached (err code: EINVAL)." << std::endl;
	}else{
		std::cout << __FUNCTION__ << ": Try to join thread #" << exitWithErr
						  << ". Unexpected return code: " << joinRes << std::endl;
	}

	/*** Test of crashing thread *************************************************
	printf(" << "******* main: creating thread %id\n", 1);
	pthread_t crashingThId;
	rc = pthread_create(&crashingThId, NULL, testCrashingThread, (void *)1);
	if (rc){
		printf("ERROR; return code from pthread_create() is %d\n", rc);
		exit(-1);
	}

	/*** Test of interrupted thread with cleanup ***********************************
	pthread_t cleanUpThId;
	pthread_attr_t thrAttr_cleanUp;
	pthread_attr_init(&thrAttr_cleanUp);
	pthread_attr_setdetachstate(&thrAttr_cleanUp,PTHREAD_CREATE_DETACHED);
	rc = pthread_create(&cleanUpThId, &thrAttr_cleanUp, testCleanUpThread, (void *)0);
	pthread_attr_destroy(&thrAttr_cleanUp);
	std::cout << "******* " << "In main: Test of interrupted thread with cleanup." << std::endl;

	if (rc){
		printf("ERROR; return code from pthread_create() is %d\n", rc);
		exit(-1);
	}

	// Stop the thread before it reaches the end of its execution
	sleep(4);
	pthread_cancel(cleanUpThId);

	// try to get the thread return code
    // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	/*** Test of cleanup thread with lock ****************************************
	pthread_t cleanUpLock;
	pthread_attr_t thrAttr_cleanUpLock;
	pthread_attr_init(&thrAttr_cleanUpLock);
	pthread_attr_setdetachstate(&thrAttr_cleanUpLock,PTHREAD_CREATE_DETACHED);
	rc = pthread_create(&cleanUpLock, &thrAttr_cleanUpLock, testCleanUpLock, (void *)0);
	pthread_attr_destroy(&thrAttr_cleanUpLock);
	std::cout << "******* " << __FUNCTION__ << ": Test of cleanup thread with lock." << std::endl;

	if (rc){
		printf("ERROR; return code from pthread_create() is %d\n", rc);
		exit(-1);
	}

	// Stop the thread before it reaches the end of its execution
	sleep(4);
	pthread_cancel(cleanUpLock);

	// if the mutex is locked, unlock it
	pthread_mutex_lock (&m_mutex);
	std::cout << __FUNCTION__ << ": Succeed to unlock the thread!" << std::endl;
	pthread_mutex_unlock (&m_mutex);

	/*** Test catching pthread_exit event ****************************************/
    std::cout << "************************************************************************" << std::endl;
    std::cout << "******* " << __FUNCTION__ << ": Test catching pthread_exit event *******" << std::endl;
    std::cout << "************************************************************************" << std::endl;
    std::cout << "Creates a detached tread and cancel the thread." << std::endl
              << "The thread is expected to catch the termination signal" << std::endl
              << "and release the locked mutexes." << std::endl << std::endl;

	pthread_t catchPthreadExit;
	pthread_attr_t thrAttr_catchPthreadExit;
	pthread_attr_init(&thrAttr_catchPthreadExit);
	pthread_attr_setdetachstate(&thrAttr_catchPthreadExit,PTHREAD_CREATE_DETACHED);

    // Intialize the thread infos
    ThreadInfo newThrInfo ( &catchPthreadExit, 0, "catchPthreadExit");
    threadArray.push_back(newThrInfo);

    rc = pthread_create( &catchPthreadExit, &thrAttr_catchPthreadExit, farmer, &threadArray );
    pthread_attr_destroy( &thrAttr_catchPthreadExit );

    std::cout << __FUNCTION__ << ": Run the thread Id: 0x" << std::hex << catchPthreadExit << std::dec << std::endl;

	if (rc){
		printf("ERROR; return code from pthread_create() is %d\n", rc);
		exit(-1);
	}

	// Stop the thread before it reaches the end of its execution
	// The purpose of this test is to catch the "abi::__forced_unwind" event.
	sleep(4);
	pthread_cancel(catchPthreadExit);

    sleep(4);
    std::cout << std::endl;



    /*** Exit Main **********************************************************/
    std::cout << "************************************************************************" << std::endl;
    std::cout << "******* " << __FUNCTION__ << ": exit main. *******" << std::endl;

	/* Last thing that main() should do */
	pthread_mutex_destroy(&m_mutex);
	pthread_exit(NULL);
}
