TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += src/threadDependency.cpp

LIBS    += -lpthread
